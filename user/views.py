from django.http import HttpResponse
from projects.models import ProjectCategory
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.conf import settings
import urllib
import json
import os

import agreelance.settings
from .forms import SignUpForm

SITE_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify'
RECAPTCHA_RESPONSE_PARAM = 'g-recaptcha-response'
RECAPTCHA_SITE_KEY = settings.RECAPTCHA_SITE_KEY

def index(request):
    return render(request, 'base.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            recaptcha_response = request.POST.get(RECAPTCHA_RESPONSE_PARAM)
            recaptcha_result = verify_recaptcha(recaptcha_response)
            if not recaptcha_result['success']:
                messages.error(request, 'Invalid reCAPTCHA. Please try again.')
            else:
                user = form.save()
                user.refresh_from_db()

                user.profile.company = form.cleaned_data.get('company')

                user.is_active = False
                user.profile.categories.add(*form.cleaned_data['categories'])
                user.save()
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(
                    username=user.username, 
                    password=raw_password
                )
                messages.success(
                    request, 
                    'Your account has been created and is awaiting verification'
                )
                return redirect('home')
    else:
        form = SignUpForm()
        
    return render(
        request, 
        'user/signup.html', 
        {'form': form, 'captcha_sitekey': RECAPTCHA_SITE_KEY}
    )

def verify_recaptcha(recaptcha_response):
    url = SITE_VERIFY_URL
    values = {
        'secret': agreelance.settings.RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }

    data = urllib.parse.urlencode(values).encode()
    req =  urllib.request.Request(url, data=data)
    response = urllib.request.urlopen(req)
    result = json.loads(response.read().decode())

    return result
