from django.test import TestCase
from .views import *
import requests
import responses
from django.test.client import RequestFactory
from django.contrib import messages
import agreelance.settings

# Create your tests here.
class SignUpTest(TestCase):

	filled_form = {
		"csrfmiddlewaretoken": "K4xUJeb5AExVqMqUC7X2DYfXNrqWQEFK5MJRRfhKP7NJSwsNItIitGOkzWchsVXr",
		"username": "ericlarsen1",
		"first_name": "Eric",
		"last_name": "Larsen",
		"categories": "1",
		"company": "Larsen Inc",
		"email": "larsen@example.com",
		"email_confirmation": "larsen@example.com",
		"password1": "qwerty123123123",
		"password2": "qwerty123123123",
		"phone_number": "12312341",
		"country": "NO",
		"state": "Troendelag",
		"city": "Trondheim",
		"postal_code": "1243",
		"street_address": "Road 1",
		"g-recaptcha-response": "03AERD8XpeB1C5hkj7jqNZe8MaWMRuMD4ObvJAyklC9t-YtMixsMDn3PetiXNWNrCUiGGWu-sLKDtn6-ltnYCurjIAzDJL68pZC_zPtTzh2ygqKLprqGeHRGZ8RVS_RGfEfzNTZEToOTQtJ1W_FiSnISIqyEaWY4mkogzAUPgWv7OwguohFQNitZUCdsnZfqIKwilfdUs7msMrcjrW5Ym_O4QlPjQRs2wdJo2Y_Tv_XGPzlFuhFmoo-ssJlBugI15G9quwK80q3QOCRH1i8Lt11Ngh9-UotHOkQrzyxmFW0vzrIuuTOCgwfR9I-9UikGnnhXd453BP7a4m1E2FOvY33Cap-Q8ha3z5AsKZgpck0lzMNiPeGG8j7IBzb0eoRqluyL0OiAQHtSJ50xZIEdXaFp7gN14sejs2V3ITXQ02zA_K6Sv6aeS4NhVMdq7bnfXqtnZvnwjD0rKA"
	}

	def test_verify_recaptcha(self):

		agreelance.settings.RECAPTCHA_SECRET_KEY = "6Ld1AN8UAAAAAGENA-uPsuCVBPydoONxPjGip2fO"
		
		old_captcha_response = self.filled_form.get('g-recaptcha-response')
		expected_result = {
			'success': False, 
			'error-codes': ['timeout-or-duplicate']
		}

		result = verify_recaptcha(old_captcha_response)
		self.assertDictEqual(result, expected_result)

	def test_signup(self):

		agreelance.settings.RECAPTCHA_SECRET_KEY = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"

		# https://stackoverflow.com/a/25835403
		rf = RequestFactory()
		get_request = rf.post('/user/signup', self.filled_form)

		# https://developers.google.com/recaptcha/docs/faq
		self.assertEqual(signup(get_request).status_code, 200)
