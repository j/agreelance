// Initialize variables
let darkModeState = false;
let body = document.body;
let toggleButton = document.getElementById('customSwitch1');

// Set default value of localstorage if not set
if(localStorage.getItem("darkmode") == "true") {
  darkModeState = true;
  darkModeOn();
} else {
  darkModeState = false;
  darkModeOff();
}

// Toggle dark mode
function darkMode() {
  if(darkModeState) {
    darkModeOff();
  } else {
    darkModeOn();
  }
}

// Turn on dark mode
function darkModeOn() {
  darkModeState = true;
  body.classList.add("darkmode");
  localStorage.setItem("darkmode", "true");
  toggleButton.checked = true
}

// Turn off dark mode
function darkModeOff() {
  darkModeState = false;
  body.classList.remove("darkmode");
  localStorage.setItem("darkmode", "false");
  toggleButton.checked = false
}
