from django.test import TestCase, Client, RequestFactory
from .views import *
from django.contrib.auth.models import User

url = "http://127.0.0.1:8000/"

#Create your tests here

#part 1
class ProjectViewTest(TestCase):

    def setUp(self):
        self.client = Client()

        self.requestFactory = RequestFactory()

        self.projectOwner = User.objects.create_user(
            pk = 1,
            username = "Project0wner",
            email = "project0wner@harakirimail.com",
            password = "morraD1erm@nn"
        )
        self.projectOwnerProfile = Profile.objects.get(user = self.projectOwner)

        self.bidder = User.objects.create_user(
            username = "13idder",
            email = "bidder@harakirimail.com",
            password = "morraD1erm@nn"
        )

        projectCategory = ProjectCategory.objects.create(pk = 1, name = "Cleaning")

        self.testProject = Project.objects.create(
            pk = 1,
            user = self.projectOwnerProfile,
            title = "Test",
            description = "Test, hjemmerens",
            category = projectCategory,
            status = "o"
        )

        self.task = Task.objects.create(
            pk = 1,
            project = self.testProject,
            title = "Test task, will pay rs3 gp",
            description = "can also do rs3 stonks",
            budget = 100
        )

    #Response
    def test_project_view(self):
        taskOffer = TaskOffer.objects.create(
            pk = 1,
            task = self.task,
            title = "Test",
            description = "dummy test",
            price = 100,
            offerer = self.bidder.profile
        )
        offerID = taskOffer.id

        self.client.login(username = "Project0wner", password = "morraD1erm@nn")
        response = self.client.post(f"/projects/{offerID}/", {
             "status" : 'a',
             "offer_response" : True,
             "taskofferid" : offerID,
             "feedback" : "It works"
        })
        self.assertEqual(response.status_code, 200)

    #notfound 404
    def test_project_not_found(self):
        taskOffer = TaskOffer.objects.create(
            pk = 1,
            task = self.task,
            title = "Test",
            description = "dummy test",
            price = 100,
            offerer = self.bidder.profile
        )
        offerID = taskOffer.id

        self.client.login(username = "Project0wner", password = "morraD1erm@nn")
        response = self.client.post(f"/notvalidpathorsumshit", {
            "status" : " ",
            "offerResponse" : False,
            "taskOfferID" : offerID,
            "feedback" : "404, site not found"
        })
        self.assertEqual(response.status_code, 404)

    #test of project view status change
    def test_view_status(self):
        request = self.requestFactory.get("/projects/" + str(self.projectOwner.id))
        request.user = self.projectOwner
        response = project_view(request, self.testProject.id)
        self.assertEqual(response.status_code, 200)

        post = self.requestFactory.post("/project/" + str(self.projectOwner.id),{
            "status_change" : "",
            "status" : "i"
        })
        post.user = self.projectOwner
        response = project_view(post, self.testProject.id)
        self.assertEqual(response.status_code, 200)

    #submit offer at project view
    def test_submit_offer(self):
        post = self.requestFactory.post("/project/" + str(self.bidder.id), {
            "offer_submit" : True,
            "title" : "testOffer",
            "price" : 100,
            "description" : "some description",
            "taskvalue" : self.task.id
        })
        post.user = self.bidder
        response = project_view(post, self.testProject.id)
        self.assertEqual(response.status_code, 200)

#part 2
class PermissionTest(TestCase):

    ownerDictionary = {
        "write" : True,
        "read" : True,
        "modify" : True,
        "owner" : True,
        "upload" : True
    }

    acceptedOfferDictionary = {
        "write" : True,
        "read" : True,
        "modify" : True,
        "owner" : False,
        "upload" : True
    }

    noPermissionDictionary = {
        "write" : False,
        "read" : False,
        "modify" : False,
        "owner" : False,
        "view_task" : False,
        "upload" : False
    }

    def setUp(self):
        projectOwner = User.objects.create(username = "Project0wner")
        profileOwner = projectOwner.profile
        projectCategory = ProjectCategory.objects.create(pk = 0, name = "Cleaning")
        project = Project.objects.create(title = "test", category = projectCategory, user = profileOwner)
        task = Task.objects.create(title = "test", project = project)
        user = User.objects.create(username = "A Offer")
        bidder = user.profile
        TaskOffer.objects.create(task = task, offerer = bidder, status = "a")

    def test_task_permission_owner(self):
        user = User.objects.get(username = "Project0wner")
        task = Task.objects.get(title = "test")
        self.assertEqual(get_user_task_permissions(user, task), self.ownerDictionary)

    def test_permission_of_bidder(self):
        user = User.objects.get(username = "A Offer")
        task = Task.objects.get(title = "test")
        self.assertEqual(get_user_task_permissions(user, task), self.acceptedOfferDictionary)

    def test_no_permission(self):
        user = User.objects.create(username = "user")
        task = Task.objects.get(title = "test")
        self.assertEqual(get_user_task_permissions(user, task), self.noPermissionDictionary)
