import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select

import os
import random 
import string
from time import sleep

url = "http://127.0.0.1:8000/"
url_login = url + "user/login/"
url_logout = url + "user/logout/"
url_new_project = url + "projects/new/"
url_projects = url + "projects/"

class AgreelanceSignUp(unittest.TestCase):

    def setUp(self):
        if os.name == "nt":
            self.driver = webdriver.Chrome("./bin/chromedriver.exe")
        else:
            self.driver = webdriver.Chrome("./bin/chromedriver")

    def test_offers(self):
        self.login("admin", "qwerty123")
        project_name_1 = self.rString(20)
        project_name_2 = self.rString(20)
        project_name_3 = self.rString(20)
        self.new_project(project_name_1)
        self.new_project(project_name_2)
        self.new_project(project_name_3)
        self.logout()

        self.login("joe", "qwerty123")
        self.apply_project(project_name_1, self.rDigits(10))
        self.apply_project(project_name_2, self.rDigits(10))
        self.apply_project(project_name_3, self.rDigits(10))
        self.logout()

        self.login("admin", "qwerty123")

        self.send_response(project_name_1, 0)
        self.send_response(project_name_2, 1)
        self.send_response(project_name_3, 2)

        print("Accepted " + project_name_1)
        print("Pending " + project_name_2)
        print("Declined " + project_name_3)

        sleep(1)

    def login(self, username, password):
        driver = self.driver
        driver.get(url_login)

        driver.find_element_by_id("id_username").send_keys(username)
        driver.find_element_by_id("id_password").send_keys(password)
        driver.find_element_by_xpath("/html/body/div/form/button").click()
        sleep(1)

    def send_response(self, project_title, status_index):
        driver = self.driver
        driver.get(url)

        project = driver.find_element_by_xpath("//*[contains(text(), \"" + project_title + "\")]")
        project.click()

        sleep(1)

        driver.find_element_by_xpath("/html/body/div/li/div/button").click()
        
        sleep(1)

        Select(driver.find_element_by_id("id_status")).select_by_index(status_index)
        driver.find_element_by_id("id_feedback").send_keys("FEEDBACK_TEXT")

        driver.find_element_by_xpath("//*[contains(text(), \"Send Response\")]")

        sleep(1)

    def apply_project(self, project_title, price):
        driver = self.driver

        driver.get(url_projects)
        sleep(1)
        project = driver.find_element_by_xpath("//*[contains(text(), \"" + project_title + "\")]")
        project.click()

        sleep(1)

        driver.find_element_by_xpath("/html/body/div/ul/li/button").click()

        sleep(1)

        driver.find_element_by_xpath("//*[@id=\"id_title\"]").send_keys("TASK_TITLE")
        driver.find_element_by_xpath("//*[@id=\"id_description\"]").send_keys("TASK_DESCRIPTION")
        driver.find_element_by_xpath("//*[@id=\"id_price\"]").send_keys(price)
        driver.find_element_by_name("offer_submit").click()

        sleep(1)


        try: 
            return "Pending" in driver.find_element_by_xpath("/html/body/div/ul/li/div/button").text
        except Exception:
            return False

    def logout(self):
        driver = self.driver
        driver.get(url_logout)
        sleep(1)

    def new_project(self, title):
        driver = self.driver

        driver.get(url_new_project)
        sleep(1)

        driver.find_element_by_id("id_title").send_keys(title)
        driver.find_element_by_id("id_description").send_keys(title)
        Select(driver.find_element_by_id("id_category_id")).select_by_index(1)
        driver.find_element_by_xpath("/html/body/div/form/div[4]/div/p[1]/label/input").send_keys("TASK_TITLE")
        driver.find_element_by_id("budget_input").send_keys(1000)
        driver.find_element_by_xpath("/html/body/div/form/div[4]/div/p[3]/label/textarea").send_keys("TASK_DESCRIPTION")

        driver.find_element_by_xpath("/html/body/div/form/div[5]/div[2]/button").click()
        sleep(1)


    def tearDown(self):
        self.driver.close()

    @staticmethod
    def rString(length):
        return ''.join(random.choices(string.ascii_lowercase, k=length))

    @staticmethod
    def rDigits(length):
        return ''.join(random.choices(string.digits, k=length))

if __name__ == "__main__":
    unittest.main()
