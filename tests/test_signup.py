# This test will only pass if the site and secret keys are set to 
# RECAPTCHA_SITE_KEY = 6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI
# RECAPTCHA_SECRET_KEY = 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe
# in agreelance/settings.py. See recaptcha docs for more information:
# https://developers.google.com/recaptcha/docs/faq

import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import time
import os
import random 
import string
from time import sleep

url = "http://127.0.0.1:8000/user/signup"

class AgreelanceSignUp(unittest.TestCase):

    def setUp(self):
        if os.name == "nt":
            self.driver = webdriver.Chrome("./bin/chromedriver.exe")
        else:
            self.driver = webdriver.Chrome("./bin/chromedriver")
    
    def test_two_way_postal_code_city(self):
        driver = self.driver
        values = self.getNominalValues()

        combinations = [
            ["Oslo", "0378", True],
            ["Oslo", "7018", False],
            ["Trondheim", "0378", False],
            ["Trondheim", "7018", True],
        ]

        for combination in combinations:
            values["city"] = combination[0]
            values["postal_code"] = combination[1]
            self.assertTrue(self.case_tester(combination) == combination[2])

    def test_two_way_city_country(self):
        driver = self.driver
        values = self.getNominalValues()

        combinations = [
            ["Oslo", "Norway", True],
            ["Oslo", "Sweden", False],
            ["Stockholm", "Norway", False],
            ["Stockholm", "Sweden", True],
        ]

        for combination in combinations:
            values["city"] = combination[0]
            values["country"] = combination[1]
            self.assertTrue(self.case_tester(combination) == combination[2])

    def test_two_way_password_verify(self):
        driver = self.driver
        values = self.getNominalValues()

        combinations = [
            ["qwerty123", "Qwerty123", False],
            ["qwerty123", "qwerty123", True],
        ]

        for combination in combinations:
            values["password"] = combination[0]
            values["password_confirmation"] = combination[1]
            self.assertTrue(self.case_tester(combination) == combination[2])

    def test_username(self):
        testcases = [
            ["username", "", False], # min-
            ["username", self.rString(1), True], # min
            ["username", self.rString(150), True], # max
            ["username", self.rString(151), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))
    
    def test_firstname(self):
        testcases = [
            ["firstname", "", False], # min-
            ["firstname", self.rString(1), True], # min
            ["firstname", self.rString(30), True], # max
            ["firstname", self.rString(31), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_lastname(self):
        testcases = [
            ["lastname", "", False], # min-
            ["lastname", self.rString(1), True], # min
            ["lastname", self.rString(30), True], # max
            ["lastname", self.rString(31), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))
    
    def test_categories(self):
        testcases = [
            ["categories", None, False], # min-
        ]

        self.assertTrue(self.case_tester(testcases))
    
    def test_company(self):
        testcases = [
            ["company", "", True], # min
            ["company", "30", True], # max
            ["company", "30", False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_email(self):

        email_6 = self.rString(1) + "@" + self.rString(1) + ".co"
        email_254 = self.rString(248) + "@" + self.rString(1) + ".com"
        email_255 = self.rString(248) + "@" + self.rString(2) + ".com"

        testcases = [
            ["email", "", False], # min-
            ["email", email_6, True], # min
            ["email", email_254, True], # max
            ["email", email_255, False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_phone_number(self):
			
        testcases = [
            ["phone_number", "", False], # min-
            ["phone_number", self.rDigits(1), True], # min
            ["phone_number", self.rDigits(50), True], # max
            ["phone_number", self.rDigits(51), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_country(self):
	
        testcases = [
            ["country", "", False], # min-
            ["country", self.rString(1), True], # min
            ["country", self.rString(50), True], # max
            ["country", self.rString(51), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_state(self):
		
        testcases = [
            ["state", "", False], # min-
            ["state", self.rString(1), True], # min
            ["state", self.rString(50), True], # max
            ["state", self.rString(51), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_city(self):
			
        testcases = [
            ["city", "", False], # min-
            ["city", self.rString(1), True], # min
            ["city", self.rString(50), True], # max
            ["city", self.rString(51), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_postal_code(self):
				
        testcases = [
            ["postal_code", "", False], # min-
            ["postal_code", self.rDigits(1), True], # min
            ["postal_code", self.rDigits(50), True], # max
            ["postal_code", self.rDigits(51), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))

    def test_street_address(self):
					
        testcases = [
            ["street_address", "", False], # min-
            ["street_address", self.rString(1), True], # min
            ["street_address", self.rString(50), True], # max
            ["street_address", self.rString(51), False], # max+
        ]

        self.assertTrue(self.case_tester(testcases))


    def case_tester(self, testcases):
        driver = self.driver

        for case in testcases:
            values = self.getNominalValues()
            values[case[0]] = case[1]
            formstate = self.fill_form(values)
            if formstate != case[2]: 
                print(values)
                return False
        return True

    
    def fill_form(self, values):
        driver = self.driver
        driver.get(url)

        username = driver.find_element_by_id("id_username")
        firstname = driver.find_element_by_id("id_first_name")
        lastname = driver.find_element_by_id("id_last_name")
        categories = Select(driver.find_element_by_id("id_categories"))
        company = driver.find_element_by_id("id_company")
        email = driver.find_element_by_id("id_email")
        email_confirmation = driver.find_element_by_id("id_email_confirmation")
        password = driver.find_element_by_id("id_password1")
        password_confirmation = driver.find_element_by_id("id_password2")
        phone_number = driver.find_element_by_id("id_phone_number")
        country = driver.find_element_by_id("id_country")
        state = driver.find_element_by_id("id_state")
        city = driver.find_element_by_id("id_city")
        postal_code = driver.find_element_by_id("id_postal_code")
        street_address = driver.find_element_by_id("id_street_address")
        
        recaptcha = driver.find_element_by_class_name("g-recaptcha")
        submit = driver.find_element_by_xpath("/html/body/div[1]/form/button")

        username.send_keys(values["username"])
        firstname.send_keys(values["firstname"])
        lastname.send_keys(values["lastname"])
        if values["categories"] != None: 
            categories.select_by_index(values["categories"])
        company.send_keys(values["company"])
        email.send_keys(values["email"])
        email_confirmation.send_keys(values["email"])
        password.send_keys(values["password"])
        password_confirmation.send_keys(values["password"])
        phone_number.send_keys(values["phone_number"])
        country.send_keys(values["country"])
        state.send_keys(values["state"])
        city.send_keys(values["city"])
        postal_code.send_keys(values["postal_code"])
        street_address.send_keys(values["street_address"])
        # recaptcha.click()
        submit.click()
        
        timeout = 1
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'alert'))
            WebDriverWait(driver, timeout).until(element_present)
            alert = driver.find_element_by_class_name("alert")
            return "Your account has been created" in alert.text
        except TimeoutException:
            # Timed out
            return False

    def getNominalValues(self):

        email_ = self.rString(10) + "@" + self.rString(10) + ".com"
        password_ = self.rString(20)

        values = {
            "username": self.rString(20),
            "firstname": self.rString(20),
            "lastname": self.rString(20),
            "categories": 0,
            "company": self.rString(20),
            "email": email_,
            "email_confirmation": email_,
            "password": password_,
            "password_confirmation": password_,
            "phone_number": self.rDigits(8),
            "country": self.rString(20),
            "state": self.rString(20),
            "city": self.rString(20),
            "postal_code": self.rDigits(4),
            "street_address": self.rString(20)
        }

        return values
        
    def tearDown(self):
        self.driver.close()

    @staticmethod
    def rString(length):
        return ''.join(random.choices(string.ascii_lowercase, k=length))

    @staticmethod
    def rDigits(length):
        return ''.join(random.choices(string.digits, k=length))

if __name__ == "__main__":
    unittest.main()
