import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os

url = "http://127.0.0.1:8000/"

# Testing of DarkMode functionality with Unittest: https://docs.python.org/3/library/unittest.html 
# and using end-to-end testing framework Selenium: https://selenium-python.readthedocs.io/
class AgreelanceDarkMode(unittest.TestCase):

    def setUp(self):
        if os.name == "nt":
            self.driver = webdriver.Chrome("./bin/chromedriver.exe")
        else:
            self.driver = webdriver.Chrome("./bin/chromedriver")

    #Checks that toggleelement should be availeble through togglebar
    def test_dark_mode_in_toggle(self):
        driver = self.driver
        driver.get(url)
        self.assertNotIn(driver.find_element_by_class_name("custom-control-label"), driver.find_elements_by_class_name("navbar navbar-expand-lg navbar-light bg-light"))

    #Find the darkmode elements
    def test_dark_mode_elements(self):
        driver = self.driver
        driver.get(url)

        darkmode_control_label = driver.find_element_by_class_name("custom-control-label")
        darkmode_control_input = driver.find_element_by_class_name("custom-control-input")

        self.assertTrue(darkmode_control_label)
        self.assertTrue(darkmode_control_input)
    
    #Checks if CSS changes if button is clicked
    def test_dark_mode_clicked(self):
        driver = self.driver
        driver.get(url)
        
        # Try to open navbar in case the screen is small
        try:
            toggler = driver.find_element_by_class_name("navbar-toggler")
            toggler.click()
            time.sleep(1)
        except Exception:
            pass

        darkmode_click = driver.find_element_by_class_name("custom-control-label")
        darkmode_click.click()
        time.sleep(1)

        self.assertEqual(driver.find_element_by_tag_name("body").value_of_css_property("background-color"), "rgba(0, 0, 0, 1)")
    

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
