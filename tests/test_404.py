import unittest
import os
from selenium import webdriver

url = "http://tdt4237.herokuapp.com/nonexistentpage/"

class AgreelanceSignUp(unittest.TestCase):

    def setUp(self):
        if os.name == "nt":
            self.driver = webdriver.Chrome("./bin/chromedriver.exe")
        else:
            self.driver = webdriver.Chrome("./bin/chromedriver")

    def test_404_image(self):
        driver = self.driver
        driver.get(url)

        image_element = driver.find_element_by_xpath("/html/body/div/img")

        image_source = image_element.get_attribute("src")

        self.assertTrue(image_source == "https://248.no/img/cat.gif")

    def test_404_text(self):
        driver = self.driver
        driver.get(url)

        header_element = driver.find_element_by_xpath("/html/body/div/h2")

        header_text = header_element.text

        self.assertTrue(header_text == "Let’s agree to disagree, this page does not exist.")

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
